(function () {
    'use strict';

    var express = require('express');
    var apiRouter = express.Router();

    apiRouter.get('/', function(req, resp){
        resp.send('mary nails API version 1.0 ');
    });

    module.exports = apiRouter;
})();