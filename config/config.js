/**
 * Created by lesne on 17.03.2016.
 */

module.exports = {
    PORT: 3000,
    HOST: 'localhost',
    apiVersion: 'v1',
    frontendPath: 'static'
};