/**
 * Created by lesne on 17.03.2016.
 */
(function () {
    'use strict';

    const config = require('./config/config');
    const env = process.env; 
    var express = require('express');
    var bodyParser = require('body-parser');
    var multer = require('multer');
    var path = require('path');

    var app = express();
    var apiRouter = require('./api/apiRouter');

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.use(express.static(path.join(__dirname, config.frontendPath)));


    app.get('/', function (req, resp) {
        resp.sendfile(path.join(__dirname, config.frontendPath) + '/index.html')
    });

    app.use('/api/' + config.apiVersion, apiRouter);



    // IMPORTANT: Your application HAS to respond to GET /health with status 200
    //            for OpenShift health monitoring
    app.get('/health', function (req, resp) {
        resp.writeHead(200);
        resp.end();
    });

    app.use(apiRouter);

    app.listen(env.NODE_PORT || config.PORT, env.NODE_IP || 'localhost', function(){
        //log.info('Express server listening on port 1337');
    });
})();
