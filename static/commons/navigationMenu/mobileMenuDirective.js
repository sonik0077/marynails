/**
 * Created by Lesneu Pavel on 12.04.2016.
 */
(function () {
    'use strict';
    
    angular.module('app').directive('navigationMenu', Menu);

    function Menu() {
        
        return {
            restrict: 'A',
            link: function (scope, element) {
                
            }
        };
        
    }

})();