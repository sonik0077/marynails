/**
 * Created by lesne on 03.04.2016.
 */
(function () {
    'use strict';

    angular.module('app').directive('homeWidget', homeWidget);

    function homeWidget() {

        return {
            restrict: 'A',
            transclude: true,
            scope: {
                model: '='
            },
            templateUrl: 'commons/widget/homeWidget.html'
        }

    }
    
})();