
/**
 * Created by Lesneu Pavel on 09.04.2016.
 */
(function () {
    'use strict';

    angular.module('app').directive('loading', LoadingDirective);

    LoadingDirective.$inject = ['$parse'];

    function LoadingDirective() {
        return {
            restrict: 'A',
            replace: false,
            transclude: true,
            ngClass: 'loading',
            template: '<div ng-class="{loading: showLoader}"><div ng-class="{\'loading-background\': showLoader}"><a ng-class="{\'loading-background-image\': showLoader}"></a></div><div ng-transclude=""></div></div>',
            link: function (scope, element, attributes) {
                scope.$watch(attributes.show, function (value) {
                    scope.showLoader = value;
                    console.log(scope.showLoader);
                })
            }
        }
    }
})();