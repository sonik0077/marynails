/**
 * Created by lesne on 29.03.2016.
 */
(function () {
    'use strict';

    angular.module('app').directive('googleMap', mapDirective);
    angular.module('app').controller('googleMapController', googleMapController);

    function mapDirective() {
        return {
            restrict: 'A',
            scope: {
                markerTitle: '@',
                additionalText: '@',
                zoom: '@',
                latitude: '@',
                longitude: '@',
                loader: '='
            },
            transclude: true,
            templateUrl: 'commons/map/map.html',
            controller: 'googleMapController',
            link: function (scope, element) {

                scope.loader = true;

                System.config({
                    meta: {
                        "google": {
                            "scriptLoad": true,
                            "format": "global",
                            "exports": "google"
                        }
                    },
                    map: {
                        "google": "//maps.googleapis.com/maps/api/js"
                    }
                });

                System.import('google').then(function () {
                    var mapDiv = element.find('.google-map')[0];
                    var settings = scope.getMapSettings();
                    var map = new google.maps.Map(mapDiv, settings.options);
                    var marker = new google.maps.Marker({
                        position: settings.options.center,
                        map: map,
                        draggable: false
                    });
                    google.maps.event.addListener(marker, 'click', function() {
                        settings.info.open(map, marker);
                    });
                    map.setOptions({
                        styles: settings.styles
                    });

                    scope.loader = false;
                    scope.$apply();
                });
            }
        }
    }

    googleMapController.$inject = ['$scope'];
    function googleMapController($scope) {

        $scope.getMapSettings = function () {
            var settings = {
                home: {
                    latitude: $scope.latitude,
                    longitude: $scope.longitude
                },
                text: '<div class="map-popup"><h4>' + $scope.markerTitle + '</h4><p>' + $scope.additionalText + '</p></div>',
                zoom: parseInt($scope.zoom)

            };
            var coords = new google.maps.LatLng(settings.home.latitude, settings.home.longitude);

            var options = {
                zoom: settings.zoom,
                center: coords,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                panControl: true,
                disableDefaultUI: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.DEFAULT
                },
                overviewMapControl: true
            };

            var info = new google.maps.InfoWindow({
                content: settings.text
            });

            var styles = [{
                "featureType": "landscape",
                "stylers": [{
                    "saturation": -100
                }, {
                    "lightness": 65
                }, {
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi",
                "stylers": [{
                    "saturation": -100
                }, {
                    "lightness": 51
                }, {
                    "visibility": "simplified"
                }]
            }, {
                "featureType": "road.highway",
                "stylers": [{
                    "saturation": -100
                }, {
                    "visibility": "simplified"
                }]
            }, {
                "featureType": "road.arterial",
                "stylers": [{
                    "saturation": -100
                }, {
                    "lightness": 30
                }, {
                    "visibility": "on"
                }]
            }, {
                "featureType": "road.local",
                "stylers": [{
                    "saturation": -100
                }, {
                    "lightness": 40
                }, {
                    "visibility": "on"
                }]
            }, {
                "featureType": "transit",
                "stylers": [{
                    "saturation": -100
                }, {
                    "visibility": "simplified"
                }]
            }, {
                "featureType": "administrative.province",
                "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "lightness": -25
                }, {
                    "saturation": -100
                }]
            }, {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "hue": "#ffff00"
                }, {
                    "lightness": -25
                }, {
                    "saturation": -97
                }]
            }];

            return {
                styles: styles,
                options: options,
                info: info
            };
        }
    }
})();