/**
 * Created by lesne on 27.03.2016.
 */
(function () {
    'use strict';

    angular.element(document).ready(function () {
        angular.bootstrap(document, ["app"]);
    });
})();