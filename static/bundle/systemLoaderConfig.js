(function () {

    function getAppConfig() {
        return {
            baseURL: '/',
            map: {
                'jquery': 'vendor/jquery-2.1.1/jquery.min.js',
                'angular': 'vendor/angularjs/angular.min.js',
                'angular-route': 'vendor/angularjs/angular-route.min.js',
                'bootstrap': 'vendor/bootstrap-3.1.1/css/bootstrap.min.css',
                'app': 'components/app/app.js',
                'bootstrap.js': 'vendor/bootstrap-3.1.1/js/bootstrap.min.js',
                'definitions': 'commons/definitions/definitions.js',
                'ngAnimate': 'vendor/angularjs/angular-animate.min.js'
            },
            meta: {
                'angular': {
                    format: 'global',
                    deps: ['jquery']
                },
                'angular-route': {
                    deps: ['angular']
                },
                'app': {
                    deps: ['angular', 'angular-route', 'bootstrap.js', 'definitions', 'ngAnimate']
                }
            }
        };
    }

    function getControllersConfig() {
        return {
            map: {
                'HeaderController': 'components/app/header/HeaderController.js',
                'ContactsController': 'components/contacts/contactsController.js',
                'HomeController': 'components/home/homeController.js'
            },
            meta: {
                'HeaderController': {
                    deps: ['ContactsController', 'HomeController']
                }
            }
        };
    }

    function getCommonsConfig() {
        return {
            map: {
                'mapDirective': 'commons/map/mapDirective.js',
                'homeWidgetDirective': 'commons/widget/homeWidgetDirective.js',
                'loaderDirective': 'commons/loader/loadingDirective.js'
            },
            meta: {
                'mapDirective': {
                    deps: ['homeWidgetDirective', 'loaderDirective']
                }
            }
        };
    }

    System.config(getAppConfig());
    System.import('app').then(function () {

        System.config(getCommonsConfig());
        System.import('mapDirective').then(function () {
            System.config(getControllersConfig());
            System.import('HeaderController').then(function () {

                System.import('appRun.js');
            });
        })
    });

})();