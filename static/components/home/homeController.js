/**
 * Created by lesne on 03.04.2016.
 */
(function () {
    'use strict';

    angular.module('app').controller('HomeController', HomeController);

    HomeController.$inject = ['$scope'];

    function HomeController($scope) {

        var self = this;

        $scope.model = {};
        $scope.local = {
            loading: true
        };

        $scope.init = function () {
            $scope.model.widgetContents = self.getWidgetContents();

            $scope.local.loading = false;
        };

        self.getWidgetContents = function () {
            return [
                {title: 'Tutorial: How to create amazing pics for nails', backgroundLink: 'http://onehdwallpaper.com/wp-content/uploads/2015/07/Nail-Art-Photos-Free-Download.jpg'},
                {title: 'The history of one client', backgroundLink: 'http://beaut-nails.ru/pig/foto_shellac1.jpg'},
                {title: 'New nails in Gallery', backgroundLink: 'http://allforfashiondesign.com/wp-content/uploads/2013/04/nails-24.jpg'},
                {title: 'Tutorial: How to create amazing pics for nails', backgroundLink: 'http://images6.fanpop.com/image/photos/34800000/Converse-converse-34865372-640-360.jpg'},
                {title: 'The history of one client', backgroundLink: 'http://onehdwallpaper.com/wp-content/uploads/2015/07/Beautiful-hands-with-Beautiful-Nails.jpg'},
                {title: 'New nails in Gallery', backgroundLink: 'http://www.georgecarvalho.com/wp-content/uploads/2015/09/Top-10-DIY-Manicure-for-beautiful-nails.png'}
            ]
        }

    }
})();