(function () {
    'use strict';

    angular.module('app', ['ngRoute', 'ngAnimate']);

    angular.module('app').config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'components/home/home.html',
            controller: 'HomeController'
        }).when('/contacts', {
            templateUrl: 'components/contacts/contacts.html',
            controller: 'ContactsController'
        }).otherwise({
            redirectTo: '/home'
        });
    }]);
})();