/**
 * Created by lesne on 28.03.2016.
 */
(function () {
    'use strict';

    angular.module('app').controller('HeaderController', HeaderController);

    HeaderController.$inject = ['$scope', '$location'];

    function HeaderController($scope, $location) {

        $scope.location = $location;

        $scope.local = {
            menuOpened: false
        };

        $scope.toggleMenu = function () {
            $scope.local.menuOpened = !$scope.local.menuOpened;
        };
    }
})();