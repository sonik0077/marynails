/**
 * Created by lesne on 26.03.2016.
 */
(function () {
    'use strict';

    angular.module('app').controller('ContactsController', ContactsController);

    ContactsController.$inject = ['$scope'];

    function ContactsController($scope) {

        $scope.local = {
            loader: {
                map: true
            }
        };

        $scope.init = function () {
            
        };

    }
})();